package net.thebugmc.d3.structure;

import net.thebugmc.parser.expression.CharPiece;
import net.thebugmc.parser.util.FilePointer;

public class ListEnd extends CharPiece {
    public ListEnd(FilePointer ptr) {
        super(ptr, ']');
    }
}
