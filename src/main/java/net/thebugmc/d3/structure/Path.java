package net.thebugmc.d3.structure;

import net.thebugmc.parser.expression.CommentPiece;
import net.thebugmc.parser.expression.ExpressionPiece;
import net.thebugmc.parser.expression.PieceResult;
import net.thebugmc.parser.util.FilePointer;

import static net.thebugmc.parser.expression.PieceResult.*;

public class Path extends ExpressionPiece {
    StringBuilder path = new StringBuilder();
    boolean space = false;
    boolean comment = false;
    char last;
    
    public Path(FilePointer ptr) {
        super(ptr);
    }
    
    public PieceResult read(char c, FilePointer ptr) {
        // path always ends on \n
        if (c == '\n') return REPLACE_LEAVE; // leave the character for CharPiece to consume
        // if there are only spaces before \n, replace to LevelStart and expect path
        
        // Check comment
        if (last == '/' && (c == '/' || c == '*')) {
            path.deleteCharAt(path.length() - 1); // remove last
            last = c;
            comment = true;
            return REPLACE_TAKE;
        }
        
        // Read more spaces...
        if (Character.isWhitespace(c)) {
            space = true;
            return CONTINUE;
        }
        
        // Chars other than space before \n, stay as path and expect value
        if (space) return LEAVE;
        
        // Just add a character to path
        path.append(last = c);
        return CONTINUE;
    }
    
    public ExpressionPiece replace(FilePointer ptr) {
        if (comment) {
            comment = false;
            return new CommentPiece(ptr, last == '/', this);
        }
        var path = path();
        if (path.isEmpty()) return null;
        if (path.equals("<")) return new LevelEnd(ptr);
        if (last == '[') return new ListStart(ptr, path.substring(0, path.length() - 1));
        return new LevelStart(ptr, path);
    }
    
    public String path() {
        return path.toString();
    }
}
