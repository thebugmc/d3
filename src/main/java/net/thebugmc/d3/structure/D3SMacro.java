package net.thebugmc.d3.structure;

import net.thebugmc.parser.util.FilePointer;

import java.util.List;

public class D3SMacro extends D3SValue {
    private final List<D3Sentence> items;

    public D3SMacro(FilePointer ptr, String path, List<D3Sentence> items) {
        super(ptr, path, null);
        this.items = items;
    }

    public List<D3Sentence> items() {
        return items;
    }
}
