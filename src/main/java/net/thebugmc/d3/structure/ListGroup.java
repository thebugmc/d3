package net.thebugmc.d3.structure;

import net.thebugmc.parser.expression.ExpressionPiece;
import net.thebugmc.parser.expression.GroupPiece;
import net.thebugmc.parser.util.FilePointer;

import java.util.List;

public class ListGroup extends GroupPiece {
    String path;
    
    public ListGroup(FilePointer ptr, List<ExpressionPiece> content, String path) {
        super(ptr, content);
        this.path = path;
    }
    
    public String path() {
        return path;
    }
}
