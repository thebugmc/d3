package net.thebugmc.d3.structure;

import net.thebugmc.parser.expression.CharPiece;
import net.thebugmc.parser.util.FilePointer;

public class Separator extends CharPiece {
    public Separator(FilePointer ptr) {
        super(ptr, ',');
    }
}