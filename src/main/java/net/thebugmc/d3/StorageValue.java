package net.thebugmc.d3;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import static java.util.Arrays.stream;
import static java.util.Optional.of;
import static net.thebugmc.error.Result.tryGet;

public record StorageValue(String value) {
    private static <T> Optional<T> iae(String s, Function<String, T> f) {
        return tryGet(
            IllegalArgumentException.class,
            () -> f.apply(s))
            .ok();
    }

    /**
     * Turn the value into its string representation.
     *
     * @return Basically, value.
     */
    public String toString() {
        return value;
    }

    //
    // Primitives
    //

    /**
     * Try to convert the value into a string.
     *
     * @return Stored string converted to string.
     */
    public Optional<String> findString() {
        return of(value);
    }

    /**
     * Try to convert the value into a boolean.
     *
     * @return Stored string converted to boolean. (should be true, case-insensitive)
     */
    public Optional<Boolean> findBoolean() {
        return iae(value, "true"::equalsIgnoreCase);
    }

    /**
     * Try to convert the value into a string.
     *
     * @return Stored string converted to a string... Yeah.
     */
    public Optional<Byte> findByte() {
        return iae(value, Byte::decode);
    }

    /**
     * Try to convert the value into a short.
     *
     * @return Stored string converted to short.
     */
    public Optional<Short> findShort() {
        return iae(value, Short::decode);
    }

    /**
     * Try to convert the value into an int.
     *
     * @return Stored string converted to int.
     */
    public Optional<Integer> findInt() {
        return iae(value, Integer::decode);
    }

    /**
     * Try to convert the value into a float.
     *
     * @return Stored string converted to float.
     */
    public Optional<Float> findFloat() {
        return iae(value, Float::parseFloat);
    }

    /**
     * Try to convert the value into a long.
     *
     * @return Stored string converted to long.
     */
    public Optional<Long> findLong() {
        return iae(value, Long::decode);
    }

    /**
     * Try to convert the value into a double.
     *
     * @return Stored string converted to double.
     */
    public Optional<Double> findDouble() {
        return iae(value, Double::parseDouble);
    }

    //
    // Lists
    //

    public static String[] splitList(String input) {
        return input.isEmpty() ? new String[0] : input.split("\n", -1);
    }

    /**
     * Convert the value into a string list.
     *
     * @return Stored string split by newlines.
     */
    public List<String> listString() {
        return findString().stream()
            .flatMap(s -> stream(splitList(s)))
            .toList();
    }

    /**
     * Convert the value into a boolean list.
     *
     * @return Stored string split by newlines converted to boolean list. (should be true, case-insensitive)
     */
    public List<Boolean> listBoolean() {
        return listString().stream()
            .map(s -> iae(s, "true"::equalsIgnoreCase).orElse(false))
            .toList();
    }

    /**
     * Convert the value into a byte list.
     *
     * @return Stored string split by newlines converted to byte list.
     */
    public List<Byte> listByte() {
        return listString().stream()
            .map(s -> iae(s, Byte::decode).orElse((byte) 0))
            .toList();
    }

    /**
     * Convert the value into a short list.
     *
     * @return Stored string split by newlines converted to short list.
     */
    public List<Short> listShort() {
        return listString().stream()
            .map(s -> iae(s, Short::decode).orElse((short) 0))
            .toList();
    }

    /**
     * Convert the value into an int list.
     *
     * @return Stored string split by newlines converted to int list.
     */
    public List<Integer> listInt() {
        return listString().stream()
            .map(s -> iae(s, Integer::decode).orElse(0))
            .toList();
    }

    /**
     * Convert the value into a float list.
     *
     * @return Stored string split by newlines converted to float list.
     */
    public List<Float> listFloat() {
        return listString().stream()
            .map(s -> iae(s, Float::parseFloat).orElse(0f))
            .toList();
    }

    /**
     * Convert the value into a long list.
     *
     * @return Stored string split by newlines converted to long list.
     */
    public List<Long> listLong() {
        return listString().stream()
            .map(s -> iae(s, Long::parseLong).orElse(0L))
            .toList();
    }

    /**
     * Convert the value into a double list.
     *
     * @return Stored string split by newlines converted to double list.
     */
    public List<Double> listDouble() {
        return listString().stream()
            .map(s -> iae(s, Double::parseDouble).orElse(0.0))
            .toList();
    }
}