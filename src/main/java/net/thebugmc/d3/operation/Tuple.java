package net.thebugmc.d3.operation;

import net.thebugmc.parser.expression.ExpressionPiece;
import net.thebugmc.parser.expression.GroupPiece;
import net.thebugmc.parser.util.FilePointer;

import java.util.List;

public class Tuple extends GroupPiece {
    public Tuple(FilePointer ptr, List<ExpressionPiece> content) {
        super(ptr, content);
    }
}