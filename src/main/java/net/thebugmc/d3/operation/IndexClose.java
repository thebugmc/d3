package net.thebugmc.d3.operation;

import net.thebugmc.parser.expression.CharPiece;
import net.thebugmc.parser.util.FilePointer;

public class IndexClose extends CharPiece {
    public IndexClose(FilePointer ptr) {
        super(ptr, ']');
    }
}
