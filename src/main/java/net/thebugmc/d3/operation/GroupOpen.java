package net.thebugmc.d3.operation;

import net.thebugmc.parser.expression.CharPiece;
import net.thebugmc.parser.util.FilePointer;

public class GroupOpen extends CharPiece {
    public GroupOpen(FilePointer ptr) {
        super(ptr, '(');
    }
}
